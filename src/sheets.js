const google = require('googleapis');
const Promise = require('bluebird');
const SHEETS_SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];

class SheetsUtils {
  constructor(parent, sheetId) {
    this._parent = parent;
    this._scopes = SHEETS_SCOPES;
    this._sheetId = sheetId;
    this._sheets = google.sheets('v4');
    this._values = this._sheets.spreadsheets.values;
  }

  _is2dArray(arr) {
    for (var i = 0; i < arr.length; i++) {
      if (!Array.isArray(arr[i])) {
        return false;
      }
    }
    return true;
  }

  get(range) {
    let self = this;
    return new Promise((resolve, reject) => {
      self._parent.authorize(self._scopes).then((auth) => {
        self._values.get({
          auth: auth,
          spreadsheetId: self._sheetId,
          range: range,
        }, (err, response) => {
          if (err) {
            return reject(err);
          }
          return resolve(response);
        });
      });
    });
  }

  append(range, values) {
    var self = this;
    if (!self._is2dArray(values)) {
      values = [values];
    }
    return new Promise((resolve, reject) => {
      self._parent.authorize(self._scopes).then((auth) => {
        self._values.append({
          auth: auth,
          spreadsheetId: self._sheetId,
          range: range,
          valueInputOption: "RAW",
          resource: {
            values: values,
          }
        }, (err, response) => {
          if (err) {
            return reject(err);
          }
          return resolve(response);
        });
      });
    });
  }
}

module.exports = SheetsUtils;
