const fs = require('fs');
const path = require('path');
const googleAuth = require('google-auth-library');
const Promise = require('bluebird');
const SheetsUtils = require('./sheets');
const DriveUtils = require('./drive');

const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
const CLIENT_SECRET_PATH = path.join(
    process.settings.__projectRoot,
    process.settings.GOOGLE_AUTH_CLIENT_SECRET_JSON_PATH || './google_client_secret.json'
)

class GoogleUtils {
  constructor() {
    this._scopes = SCOPES;
    this.CLIENT_SECRET_PATH = CLIENT_SECRET_PATH;
    this.auth_client = new googleAuth();
    this.jwt_client = {};
    this.jwt_token = {};
  }

  _getCreds() {
    return new Promise((resolve, reject) => {
      fs.readFile(CLIENT_SECRET_PATH, function processClientSecrets(err, content) {
        if (err) {
          return reject(err);
        }
        content = JSON.parse(content);
        resolve(content);
      });
    });
  }

  _serviceAccountAuth(scopes) {
    let self = this;
    return new Promise((resolve, reject) => {
      self._getCreds().then((creds) => {
        self.jwt_client = new self.auth_client.JWT(creds.client_email, null, creds.private_key, scopes || self._scopes, null);
        self.jwt_client.authorize((err, token) => {
          if (err) {
            return reject(err);
          }
          self.jwt_token = token;
          return resolve(self.jwt_client);
        });
      });
    });
  }

  _scopesAreValid(scopes) {
   if (scopes) {
     for (var i = 0; i < scopes.length; i++) {
       if (scopes[i] != this._scopes[i]) {
         return false;
       }
     }
   }
   return true;
 }

 authorize(scopes) {
   let self = this;
   return new Promise((resolve, reject) => {
     let token_valid = (self.jwt_token.expiry_date && self.jwt_token.expiry_date > Date.now());
     if (token_valid && self._scopesAreValid(scopes)) {
       // console.log("Old token still valid, returning...");
       return resolve(self.jwt_client);
     }
     // console.log("Setting new scopes and creating new token...");
     self._scopes = scopes || SCOPES;
     self._serviceAccountAuth(scopes).then((auth) => {
       return resolve(auth);
     })
   });
 }

 sheet(sheetId) {
   return new SheetsUtils(this, sheetId);
 }

 drive() {
   return new DriveUtils(this);
 }
}

module.exports = GoogleUtils;
