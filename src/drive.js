"use strict";
const google = require('googleapis');
const Promise = require('bluebird');
const DRIVE_SCOPES = ['https://www.googleapis.com/auth/drive'];

class DriveUtils {
  constructor(parent) {
    this._parent = parent;
    this._scopes = DRIVE_SCOPES;
    this._drive = google.drive('v3');
    this._files = this._drive.files;
  }

  command(commandName, options) {
    var self = this;
    return new new Promise((resolve, reject) => {
      self._parent.authorize(this._scopes).then((auth) => {
        options.auth = auth;
        self._files.[commandName](options, (err, response) => {
          if (err) {
            return reject(err);
          }
          resolve(response);
        });
      });
    });
  }

  list(options) {
    var self = this;
    return new new Promise((resolve, reject) => {
      self._parent.authorize(this._scopes).then((auth) => {
        options.auth = auth;
        self._files.list(options, (err, response) => {
          if (err) {
            return reject(err);
          }
          resolve(response);
        });
      });
    });
  }
}

module.exports = DriveUtils;
